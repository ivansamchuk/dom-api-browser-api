const url = 'https://rickandmortyapi.com/api/character';
fetch(url)
    .then(res => res.json())
    .then(data => {
        const rawData = data.results;
        return rawData.map(character => {
          //all needed data is listed below as an entity 
           let created = character.created;
           let species = character.species ;
           let img = character.image;
           let episodes = character.episode;
           let name = character.name;
           let location = character.location;
        //    console.log(character.name);
            //create element
            const characters = document.getElementById('characters');
            // name
            const item = document.createElement("div");
            item.innerHTML = "<span class='name'>" + name + "</span>";
            item.className = 'character';
            characters.append(item);
            // image
            const item2 = document.createElement('img');
            item2.classname = "character-image";
            item2.src = img;
            characters.append(item2);
            // species
            const item3 = document.createElement('span');
            item3.classname = "species";
            item3.innerText = species;
            characters.append(item3);
            // episodes
            // const item4 = document.createElement('span');
            // item3.classname = "species";
            // item3.innerText = species;
            // characters.append(item4);
            // location
            const item5 = document.createElement('span');
            item5.classname = "location";
            item5.innerText = location;
            characters.append(item5);


                    });
                })
    .catch((error) => {
        console.log(JSON.stringify(error));
    });